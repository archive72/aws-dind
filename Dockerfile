FROM amazonlinux:2.0.20210617.0 as tmp

WORKDIR /tmp

RUN yum upgrade -y && \
  yum install -y unzip && \
  amazon-linux-extras install docker -y && \
  curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
  unzip awscliv2.zip && \
  ./aws/install && \
  rm -rf awscliv2.zip aws && \
  yum remove -y unzip && \
  yum clean all && \
  rm -rf /var/cache/yum
  
FROM tmp

WORKDIR /

# RUN yum upgrade -y
# RUN yum install -y unzip
# RUN amazon-linux-extras install docker -y
# RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
# RUN unzip awscliv2.zip
# RUN ./aws/install
# RUN rm -rf awscliv2.zip install
# RUN yum remove -y unzip
# RUN yum clean all
# RUN rm -rf /var/cache/yum
